import React, { Component } from 'react';
import { BrowserRouter as Router } from 'react-router-dom';
import { routes, SubRoutes } from './routes';
import Header from './components/Header';

class App extends Component {
    render() {
        return (
            <Router>
                <div>
                    <Header />
                    {routes.map((route, i) => (
                        <SubRoutes key={i} {...route} />
                    ))}
                </div>
            </Router>
        );
    }
}

export default App;
