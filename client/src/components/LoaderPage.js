import React, { Component } from 'react';

import loading from '../images/loading.gif';
import '../styles/css/LoaderPage.css';

class LoaderPage extends Component {
    render() {
        return (
            <div className="loader">
                <img className="loader-gif" src={loading} alt="Gif de carregamento da página" />
            </div>
        );
    }
}


export default LoaderPage;
