import React, { Component } from 'react';
import db from '../config/firebase';
import '../styles/css/Game.css';

class Timeline extends Component {
    state = {
        ready: false,
        timeline: {},
    }

    componentDidMount() {
        let game = (this.props.game !== undefined ? this.props.game : this.props.match.params.id)
        console.log(game)
        db.collection('game').doc(game).collection('timeline').get()
        .onSnapshot((doc) => {
            console.log(doc)
            this.setState({
                ready: true,
                game: {
                    id: doc.id,
                    data: doc.data()
                },
            })
        })
    }

    render() {
        let content
        let { ready } = this.state

        if (ready) {

        }

        return (
            <div className="Timeline">
                { content }
            </div>
        );
    }
}

export default Timeline;
