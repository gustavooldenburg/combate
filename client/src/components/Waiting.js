import React, { Component } from 'react';
import { Row, Button } from 'react-materialize';
import api from '../routes/api';
import '../styles/css/Waiting.css';

//r4L84ZqH8iZFVp8TyKEX

class Game extends Component {
    state = {
        init: false,
        ready: false,
        game: {},
        judge: ''
    }

    constructor(props) {
        super(props)

        this.state = {
            ready: props.game.data.start.ready.includes(props.judge),
            init: props.init,
            game: props.game,
            judge: props.judge
        }
    }

    componentWillReceiveProps(nextProps) {
        console.log('teste')
        if (nextProps.game.data !== undefined) {
            this.setState({
                ready: nextProps.game.data.start.ready.includes(nextProps.judge),
                init: nextProps.init,
                game: nextProps.game.id,
                judge: nextProps.judge
            })
            console.log(this.state)
        }
    }

    onReady(ready) {
        let url = (ready ? 'game/init' : 'game/cancel-init')
        this.setState({ ready })

        api.post(url, { game: this.state.game.id, judge: this.state.judge })
        .catch(error => {
            console.log(error)
            this.setState({ ready: !ready })
        })
    }

    render() {
        let text = ''
        let waiting = ''
        if (this.state.ready) {
            text = 'Cancelar espera'
            waiting = 'Aguardando os outros juízes'
        } else {
            text = 'Iniciar partida'
            waiting = 'Pronto para iniciar?'
        }

        const style = this.state.init ? {display: 'none'} : {display: 'flex'}

        return (
            <Row className={"Waiting primary-color "+this.state.ready} style={style}>
                <div className="Waiting-text">{ waiting }</div>
                <Button waves='light' onClick={() => this.onReady(!this.state.ready)}>{ text }</Button>
            </Row>
        );
    }
}

export default Game;
