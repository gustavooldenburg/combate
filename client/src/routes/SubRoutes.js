import React, { Component } from 'react';
import { Route } from "react-router-dom";

class SubRoutes extends Component {
    constructor(props, routes) {
        super(props);
    }

    render() {
        return (
            <Route
                path={this.props.path}
                exact={true}
                render={props => (
                    <this.props.component {...props} routes={this.props.routes} />
                )}
            />
        );
    }
}

export default SubRoutes;
