import SubRoutes from './SubRoutes';

import Login from '../screen/Login';
import Game from '../screen/Game';
import Timeline from '../screen/Timeline';
import Watch from '../screen/Watch';

const routes = [{
    name: 'login',
    path: '/',
    component: Login
}, {
    name: 'game',
    path: '/game/:judge/:id',
    component: Game,
}, {
    name: 'timeline',
    path: '/timeline/:id',
    component: Timeline,
}, {
    name: 'watch',
    path: '/watch',
    component: Watch
}];

function route_path(name) {
    let path;

    routes.forEach(function(route) {
        if (route.name === name){
            path = route.path;
        }
        if (route.routes !== undefined) {
            route.routes.forEach(function(r) {
                if (r.name === name){
                    path = r.path;
                }
            })
        }
    })

    return path;
}

export {
    routes,
    SubRoutes,
    route_path,
};
