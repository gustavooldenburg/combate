import React, { Component } from 'react';
import { Row, Col, Button } from 'react-materialize';
import db from '../config/firebase';
import api from '../routes/api';
import Waiting from '../components/Waiting';
import LoaderPage from '../components/LoaderPage';
import Timeline from '../screen/Timeline';
import '../styles/css/Game.css';

//r4L84ZqH8iZFVp8TyKEX

//EFgSH2eUMc5FqSwKPbv4

class Game extends Component {
    state = {
        ready: false,
        judge: '',
        game: {},
        gameTime: 0,
        knockoutTime: 0,
    }

    interval = null;

    componentDidMount() {
        this.setState({ judge: this.props.match.params.judge })
        db.collection('game').doc(this.props.match.params.id)
        .onSnapshot((doc) => {
            this.setState({
                ready: true,
                game: {
                    id: doc.id,
                    data: doc.data()
                },
            })

            if (doc.data().start.status && this.interval === null) {
                this.interval = setInterval(() => {
                    let { start, knockout } = this.state.game.data

                    this.setState({ gameTime: this.getGameTime(start) })
                    this.setState({ knockoutTime: this.getKnockoutTime(knockout) })

                    if (this.state.gameTime.currentTime <= 0 || this.state.knockoutTime.currentTime <= 0) {
                        clearInterval(this.interval)
                        this.finishGame(( this.state.gameTime <= 0 ? 'points' : 'knockout' ))
                    }
                }, 100)
            }
        })
    }

    addAction(robot, action) {
        let { game, judge } = this.state
        let timestamp = new Date().getTime()

        api.post('game/action', { game: game.id, judge, robot, action, timestamp })
        .catch(error => {
            console.log(error)
        })
    }

    finishGame(method) {
        let game = this.state.game.id
        let { knockout, points, robots } = this.state.game.data
        let robot

        if (method === 'knockout') {
            robot = knockout.robot
        } else {
            let id, value = 0

            for (let key in points) {
                if (value < points[key]) {
                    id = key
                    value = points[key]
                }
            }

            robots.forEach(val => {
                if (val.id === id) {
                    robot = val
                }
            })
        }

        api.post('game/finish', { game, robot, method })
        .then(() => {
            // this.props.history.push('/timeline/'+game)
        })
        .catch(error => {
            console.log(error)
        })
    }

    getGameTime(start) {
        let timeFinish = start.timestamp + 300000
        let currentTime = timeFinish - new Date().getTime()
        let minutesTime = currentTime / 60000
        let minutes = parseInt(minutesTime, 10)
        let secondsTime = parseInt((minutesTime - minutes) * 60, 10)
        let seconds = ('0' + secondsTime).slice(-2)

        return { show: minutes + ':' + seconds, currentTime }
    }

    getKnockoutTime(start) {
        let timeFinish = start.timestamp + 10000
        // console.log('timef', new Date(timeFinish))
        let currentTime = timeFinish - new Date().getTime()
        // console.log('time', new Date())
        let secondsTime = parseInt(currentTime / 1000, 10)
        let seconds = ('0' + secondsTime).slice(-2)

        return { show: seconds, currentTime }
    }

    stylizeSpinner(time, base) {
        let w = time / base * 100
        let width = w + '%'
        let left = ((100 - w) / 2) + '%'

        return { width, left }
    }

    render() {
        let { ready, game, judge, gameTime, knockoutTime } = this.state
        let { robots, points, knockout, start, finish } = (ready ? this.state.game.data : {
            robots: [{ id: 0, name: 'Carregando' }, { id: 1, name: 'Carregando' }],
            points: { 0: 0, 1: 0 },
            knockout: { status: false },
            start: { status: false },
            finish: { status: false }
        })

        let content

        if (!ready) {
            content = <LoaderPage />
        } else if (!start.status) {
            content = <Waiting judge={judge} game={game} />
        } else if (finish.status) {
            content = <Timeline game={game.id} />
        } else {
            let styleTimer = this.stylizeSpinner(gameTime.currentTime, 300000)
            let styleKnockout = this.stylizeSpinner(knockoutTime.currentTime, 10000)

            content =
            <Row className="Game primary-color">
                <div className="timer">
                    <div className="timer-spin" style={styleTimer}></div>
                    { gameTime.show }
                </div>
                <Col s={6}>
                    <div className="name">{ robots[0].name }</div>
                    <div className="points">{ points[robots[0].id] }</div>
                    <Button
                        waves="light"
                        className="demage"
                        onClick={() => this.addAction(robots[0], 'demage')}>
                            Dano
                        </Button>
                    <Button
                        waves="light"
                        className="aggressiveness"
                        onClick={() => this.addAction(robots[0], 'aggressiveness')}>
                            Agressividade
                        </Button>
                    <Button
                        waves="light"
                        className="knockout"
                        onClick={() => this.addAction(robots[0], 'knockout')}>
                            Nocaute
                        </Button>
                </Col>
                <Col s={6}>
                    <div className="name">{ robots[1].name }</div>
                    <div className="points">{ points[robots[1].id] }</div>
                    <Button
                        waves="light"
                        className="demage"
                        onClick={() => this.addAction(robots[1], 'demage')}>
                            Dano
                        </Button>
                    <Button
                        waves="light"
                        className="aggressiveness"
                        onClick={() => this.addAction(robots[1], 'aggressiveness')}>
                            Agressividade
                        </Button>
                    <Button
                        waves="light"
                        className="knockout"
                        onClick={() => this.addAction(robots[0], 'knockout')}>
                            Nocaute
                        </Button>
                </Col>
                { (knockout.status ?
                <div>
                    <div className="knockout-background" />
                    <div className="timer knockout">
                        <div className="timer-spin knockout-spin" style={styleKnockout} />
                        <div className="knockout-title">Nocaute do {knockout.robot.name}</div>
                        <span>{ knockoutTime.show }</span>
                        <Button waves="light" onClick={() => this.addAction(knockout.robot, 'cancel-knockout')}>
                            Cancelar nocaute
                        </Button>
                    </div>
                </div>
                : '') }
            </Row>
        }

        return (content);
    }
}

export default Game;
