import React, { Component } from 'react';
import { Row, Col } from 'react-materialize';
import QrReader from "react-qr-reader";
import api from '../routes/api';
import '../styles/css/Login.css';

class Login extends Component {
    constructor(props) {
        super(props);

        this.handleScan = this.handleScan.bind(this);
    }

    handleScan(data) {
        if (data) {
            console.log(data)
            api.get('judge/'+data)
            .then(judge => {
                if (judge.data) {
                    this.props.history.replace('/game/'+judge.data.id+'/'+judge.data.data.game)
                }
            })
        }
    }

    handleError(err) {
        console.error(err);
    }

    render() {
        return (
            <div className="Login primary-color">
                <Row>
                    <Col s={12} className="qrcode">
                        <h4>Leia o QR-Code para ser juíz da partida</h4>
                        <QrReader
                            delay={1000}
                            onError={this.handleError}
                            onScan={this.handleScan}
                            style={{ width: '100%' }} />
                    </Col>
                </Row>
            </div>
        );
    }
}

export default Login;
