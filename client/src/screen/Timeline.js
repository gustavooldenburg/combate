import React, { Component } from 'react';
import { Row, Col, Icon, Button } from 'react-materialize';
import LoaderPage from '../components/LoaderPage';
import ScrollableAnchor, { configureAnchors } from 'react-scrollable-anchor';
import db from '../config/firebase';
import '../styles/css/Timeline.css';

class Timeline extends Component {
    state = {
        ready: false,
        timeline: {},
        game: {}
    }

    constructor(props) {
        super(props)

        configureAnchors({offset: -50})
    }

    componentDidMount() {
        let game = (this.props.game !== undefined ? this.props.game : this.props.match.params.id)

        db.collection('game').doc(game).get()
        .then((snapshot) => {
            let game = snapshot.data()
            this.setState({ game })
        })

        db.collection('game').doc(game).collection('timeline')
        .onSnapshot((snapshot) => {
            let timeline = []
            snapshot.docs.forEach(doc => {
                timeline.push(doc.data())
            })
            this.setState({
                ready: true, timeline
            })
        })
    }

    getSide(robot, action) {
        let { robots } = this.state.game
        let side = ['left', 'right']
        let i

        robots.forEach((rbt, index) => {
            if (action === 'cancel-knockout' && rbt.id !== robot) {
                i = index
            } else if (action !== 'cancel-knockout' && rbt.id === robot) {
                i = index
            }
        })

        return side[i]
    }

    getRobots(key) {
        let { robots } = this.state.game
        let rbts = ['', '']

        robots.forEach(rbt => {
            if (rbt.id === key) {
                rbts[0] = rbt.name
            } else {
                rbts[1] = rbt.name
            }
        })

        return rbts
    }

    replaceName(string, rbt) {
        return string.replace('$rb1', rbt[0]).replace('$rb2', rbt[1])
    }

    createJudges(judges) {
        let content = []

        for (let i = 0; i < 3; i++) {
            if ( i >= judges ) {
                content.push(<Icon key={i} className="judge judge-off" tiny>person</Icon>)
            } else {
                content.push(<Icon key={i} className="judge judge-on" tiny>how_to_reg</Icon>)
            }
        }
        return content
    }

    createPoints(robots, points) {
        let content = []

        robots.forEach((rbt, key) => {
            content.push(
                <div key={key}>
                    <label>{ rbt.name }</label>
                    <span>{ points[rbt.id] }</span>
                </div>
            )
        })
        return content
    }

    createActions(actions) {
        let content = []

        actions.forEach((act, key) => {
            content.push(
                <div key={key} className={'action ' + act.side + ' ' + act.valid}>
                    <div className="name">{ act.name }</div>
                    { this.createJudges(act.judges) }
                    <div className="time">{ this.formatDate(act.timestamp) }</div>
                </div>
            )
        })
        return content
    }

    formatDate(timestamp) {
        let { start } = this.state.game
        let timeFinish = start.timestamp + 300000
        let currentTime = timeFinish - timestamp
        let minutesTime = currentTime / 60000
        let minutes = parseInt(minutesTime, 10)
        let secondsTime = parseInt((minutesTime - minutes) * 60, 10)
        let seconds = ('0' + secondsTime).slice(-2)

        return minutes + ':' + seconds
    }

    render() {
        let { ready } = this.state
        let content = []
        let actions = []
        let labels = {
            'cancel-knockout': '$rb2 voltou para a partida',
            knockout: '$rb1 nocauteou $rb2',
            demage: '$rb1 danificou $rb2',
            aggressiveness: '$rb1 agrediu $rb2',
            'finish-knockout': '$rb1 ganhou por nocaute',
            'finish-points': '$rb1 ganhou por pontuação',
        }

        if (ready) {
            let { timeline } = this.state
            let { points, finish, robots, start } = this.state.game

            timeline.forEach(action => {
                let group = false

                actions.forEach((act, i) => {
                    let diff = action.timestamp - act.timestamp
                    let valid = action.action === act.action &&
                                action.judge !== act.judge &&
                                action.robot === act.robot &&
                                diff <= 30000

                    if (valid) {
                        group = true
                        actions[i].judges += 1
                        actions[i].valid = true
                        actions[i].timestamp = (actions[i].timestamp > action.timestamp ? actions[i].timestamp : action.timestamp)
                        return
                    }
                })

                if (!group) {
                    let rbts = this.getRobots(action.robot)

                    actions.push({
                        name: this.replaceName(labels[action.action], rbts),
                        side: this.getSide(action.robot, action.action),
                        valid: false, judges: 1, action: action.action,
                        timestamp: action.timestamp, judge: action.judge, robot: action.robot
                    })
                }
            })

            actions.push({
                name: this.replaceName(labels['finish-'+finish.method], [finish.robot.name, '']),
                side: 'all', valid: true, judges: 0, timestamp: finish.timestamp
            })

            actions = actions.sort((a, b) => {
                return b.timestamp - a.timestamp
            })

            actions.push({
                name: 'Início da partida',
                side: 'all', valid: true, judges: 0, timestamp: start.timestamp
            })

            content =
            <Row className="Timeline">
                <Col s={12} m={6}>
                    { (finish.status ?
                        <Col s={12} className="winner">
                            <div>🏆🎉</div> { finish.robot.name }
                        </Col> : '')
                    }
                    <Col s={12} className="points">
                        { this.createPoints(robots, points) }
                    </Col>
                    <div className="button-timeline">
                        <a href="#timeline"><div>Timeline</div>
                        <Icon>keyboard_arrow_down</Icon></a>
                    </div>
                </Col>
                <ScrollableAnchor id={'timeline'}>
                    <Col s={12} m={6} offset="m6">
                        <Col s={12} className="timelist">
                            { this.createActions(actions) }
                        </Col>
                    </Col>
                </ScrollableAnchor>
            </Row>
        } else {
            content = <LoaderPage />
        }

        return (content);
    }
}

export default Timeline;
