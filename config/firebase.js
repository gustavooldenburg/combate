const firebase = require('firebase');

const config = {
    apiKey: "AIzaSyCJ93re-h36aduPckSzll6k1lZrWFODiqw",
    authDomain: "combate-cd5d6.firebaseapp.com",
    databaseURL: "https://combate-cd5d6.firebaseio.com",
    projectId: "combate-cd5d6",
    storageBucket: "combate-cd5d6.appspot.com",
    messagingSenderId: "210247950470"
};

if (!firebase.apps.length) {
    firebase.initializeApp(config);
}

const db = firebase.firestore();
db.settings({ timestampsInSnapshots: true });

module.exports = db;
