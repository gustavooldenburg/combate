const db = require('../config/firebase');

const actions = {
    aggressiveness: 5,
    demage: 6
}

async function validAction(refs, data) {
    let { refTimeline, refGame } = refs
    let { robot, timestampInitial, timestamp, action, judge } = data
    let isValid = false
    console.log(data)

    let timeline = await refTimeline.where('action', '==', action)
        .where('robot', '==', robot.id)
        .where('timestamp', '>=', timestampInitial)
        .where('timestamp', '<=', timestamp).get()
    .then(snapshot => {
        if (snapshot.size > 0) {
            isValid = true

            snapshot.forEach(act => {
                if (act.data().judge == judge) {
                    isValid = false
                }
            })
        }
    })

    return isValid
}

async function addPoints(refs, data) {
    let { refTimeline, refGame } = refs
    let { robot, timestampInitial, timestamp, action, judge } = data
    let isAddPoint = await validAction(refs, data)

    if (isAddPoint) {
        await refGame.get().then(snap => {
            let points = snap.data().points
            points[robot.id] += actions[action]

            refGame.update({ points })
            .then(() => {
                return true
            })
            .catch((error) => {
                throw new Error('Não foi possível alterar');
            });
        })
    }

    return false
}

async function knockout(refs, data) {
    let { refTimeline, refGame } = refs
    let { robot, timestampInitial, timestamp, action, judge } = data
    let isKnockout = await validAction(refs, data)

    if (isKnockout) {
        await refGame.get().then(snap => {
            let knockout = (action == 'knockout' ? {
                knockout: { robot, timestamp: new Date().getTime(), status: true }
            } : {
                knockout: { status: false }
            })

            refGame.update(knockout)
            .then(() => {
                return true
            })
            .catch((error) => {
                throw new Error('Não foi possível alterar');
            });
        })
    }

    return false
}

module.exports = {
    init: async (req, res, next) => {
        try {
            const game = req.body.game;
            const judge = req.body.judge;

            if (!game || !judge) {
                throw new Error('Informações insuficientes');
            }

            const refGame = db.collection('game').doc(game)
            refGame.get().then(snap => {
                let ready = snap.data().start.ready

                if (!ready) {
                    ready = []
                }

                ready.push(judge)

                if (ready.length == 3) {
                    refGame.update({ start: { ready, status: true, timestamp: new Date().getTime() } })
                    .then(() => {
                        res.json({ status: true });
                    })
                    .catch((error) => {
                        throw new Error('Não foi possível alterar');
                    });
                } else {
                    refGame.update({ start: { ready, status: false, timestamp: 0 } })
                    .then(() => {
                        res.json({ status: true });
                    })
                    .catch((error) => {
                        throw new Error('Não foi possível alterar');
                    });
                }
            })
        } catch(e) {
            next(e);
        }
    },
    cancelInit: async (req, res, next) => {
        try {
            const game = req.body.game;
            const judge = req.body.judge;

            if (!game || !judge) {
                throw new Error('Informações insuficientes');
            }

            const refGame = db.collection('game').doc(game)
            refGame.get().then(snap => {
                let ready = snap.data().start.ready
                let index = ready.indexOf(judge)
                ready.splice(index, 1)

                refGame.update({ start: { ready, status: false } })
                .then(() => {
                    res.json({ status: true });
                })
                .catch((error) => {
                    throw new Error('Não foi possível alterar');
                });
            })
        } catch(e) {
            next(e);
        }
    },
    action: async (req, res, next) => {
        try {
            const game = req.body.game;
            const action = req.body.action;
            const judge = req.body.judge;
            const timestamp = req.body.timestamp;
            const robot = req.body.robot;
            let response = {}

            if (!game || !action || !judge || !timestamp || !robot) {
                throw new Error('Informações insuficientes');
            }

            let timestampInitial = timestamp - 30000
            const refGame = db.collection('game').doc(game)
            const refTimeline = refGame.collection('timeline')

            if (actions.hasOwnProperty(action)) {
                response['action'] = await addPoints({refTimeline, refGame}, {action, robot, judge, timestampInitial, timestamp})
            } else {
                response['action'] = await knockout({refTimeline, refGame}, {action, robot, judge, timestampInitial, timestamp})
            }

            refTimeline.add({ action, timestamp, robot: robot.id, judge })
            .then(() => {
                response['timeline'] =  true;
            })
            .catch((error) => {
                throw new Error('Não foi possível adicionar');
            });

            res.json(response);
        } catch(e) {
            next(e);
        }
    },
    finish: async (req, res, next) => {
        try {
            const game = req.body.game;
            const method = req.body.method;
            const robot = req.body.robot;

            if (!game || !method || !robot) {
                throw new Error('Informações insuficientes');
            }

            const refGame = db.collection('game').doc(game)
            refGame.update({
                finish: { method, robot, status: true, timestamp: new Date().getTime() }
            })
            .then(() => {
                res.json({ status: true });
            })
            .catch((error) => {
                throw new Error('Não foi possível alterar');
            });
        } catch(e) {
            next(e);
        }
    }
}
