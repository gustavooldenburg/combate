const db = require('../config/firebase');

module.exports = {
    get: async (req, res, next) => {
        try {
            const token = req.params.token
            let judge = null
            let refJudge = db.collection('judge')
            await refJudge.where('token', '==', token).get()
            .then(snapshot => {
                snapshot.forEach((doc) => {
                    let data = doc.data()

                    if (!data.open) {
                        refJudge.doc(doc.id).update({ open: true })
                        judge = {
                            id: doc.id,
                            data: doc.data()
                        }
                    }
                })
            })
            res.json(judge)
        } catch(e) {
            next(e)
        }
    }
};
