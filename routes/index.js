const express = require('express');
const { Router, Request } = require('express');
const db = require('../config/firebase');

const judge = require('../models/judge');
const game = require('../models/game');

const router = Router()

router.get('/judge/:token', judge.get)

router.post('/game/init', game.init)
router.post('/game/cancel-init', game.cancelInit)
router.post('/game/action', game.action)
router.post('/game/finish', game.finish)

module.exports = router;
